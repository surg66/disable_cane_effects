name = "Disable cane effects"
description = "Disable cane effects. You can choose disable/enable cane effects ancient/goatshead/corrupted/rosy red/fool's marott/candy skins in options."
author = "surg"
version = "1.0.5"
forumthread = ""
api_version_dst = 10
icon_atlas = "modicon.xml"
icon = "modicon.tex"
priority = 0
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false
hamlet_compatible = false
dst_compatible = true
all_clients_require_mod = false
client_only_mod = true
server_filter_tags = {""}

configuration_options =
{
    {
        name    = "CANE_ANCIENT_FX",
        label   = "Ancient cane effects",
        hover   = "Sets ancient cane effects",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = false
    },
    {
        name    = "CANE_VICTORIAN_FX",
        label   = "Goatshead cane effects",
        hover   = "Sets goatshead cane effects",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = false
    },
    {
        name    = "CANE_CANDY_FX",
        label   = "Candy cane effects",
        hover   = "Sets candy cane effects",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = false
    },
    {
        name    = "CANE_SHARP_FX",
        label   = "Corrupted cane effects",
        hover   = "Sets corrupted cane effects",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = false
    },
    {
        name    = "CANE_HARLEQUIN_FX",
        label   = "Fool's marotte cane effects",
        hover   = "Sets harlequin cane effects",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = false
    },
    {
        name    = "CANE_ROSE_FX",
        label   = "Rosy red cane effects",
        hover   = "Sets rosy red cane effects",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = false
    },
}
